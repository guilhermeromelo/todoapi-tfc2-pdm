import express, { json } from "express";
import cors from "cors";
import routes from "./routes";
const db = require("./database")

const app = express();

app.use(json());
app.use(cors());

app.get('/', (req, res) => {
  return res.send("Servidor ativo! by: Guilherme");
});

app.use('/', routes);

app.listen(8100, () => {
  console.log("🚀 Server started on http://localhost:8100");
});