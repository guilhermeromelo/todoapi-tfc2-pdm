import express from "express";
import TaskController from "./controllers/TaskController";

const routes = express.Router();

//USERS
routes.post('/tasks/new', TaskController.insert);
routes.get('/tasks', TaskController.getAll);
routes.patch('/tasks', TaskController.patch);
routes.put('/tasks/done/:bool/:id',TaskController.updateIsDone)
routes.delete('/tasks/:id', TaskController.delete);
routes.delete('/tasks/del/:id', TaskController.delete);

export default routes;