var sqlite3 = require('sqlite3').verbose()

const DBSOURCE = './src/data.db'

let db = new sqlite3.Database(DBSOURCE, (err: { message: any; }) => {
    if (err) {
        // Cannot open database
        console.error(err.message);
        throw err
    }
    else {
        console.log('Connected to the SQLite database.')
        const tabelas = [
            `CREATE TABLE TASK (
                id integer primary key AUTOINCREMENT,
                description varchar(200),
                isUrgent bool,
                isDone bool
            );`
        ]

        tabelas.forEach(tabela => {
            db.run(tabela, function (err: any) {
                if (err) {
                    console.log('Tabela já existe');
                    if (!err.message.includes("already exists")) {
                        console.log(err);
                    }
                } else {
                    console.log("Tabela TASK criada!");
                }
            });
        });
    }
});

export default db