export interface Task {
    id?: number,
    description?: string,
    isUrgent?: boolean,
    isDone?: boolean
}