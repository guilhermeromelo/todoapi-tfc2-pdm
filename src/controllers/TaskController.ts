
import { Request, Response } from "express";
import BasicAPIResponse from '../models/BasicResponse/BasicAPIResponse';
import db from "../database";
import { Task } from "../models/Task/Task";


export default class TaskController {

    static async insert(req: Request, res: Response) {
        let newTask: Task = <Task> req.body;
        console.log(req.body);

        let response = await TaskController.insertDatabase(newTask);
        let status = response.error == true ? 400 : 200;
        return res.status(status).json(response.response);
    }

    static async insertDatabase(newObject: Task): Promise<BasicAPIResponse> {
        return new Promise((resolve, reject) => {
            let query = `
            INSERT INTO TASK (
                description,
                isUrgent,
                isDone
            ) VALUES (?,?,?)
            `;
            let params = [
                newObject.description,
                newObject.isUrgent,
                newObject.isDone
            ];
            db.run(query, params, function (err: any) {
                let response: any;
                if (err) {
                    response = new BasicAPIResponse(err.stack, true);
                    reject(response);
                }
                newObject.id = this.lastID;
                response = new BasicAPIResponse(newObject, false);
                resolve(response);
            });
        });
    }

    static async getAll(req: Request, res: Response) {
        let objectArray = await TaskController.getAllDatabase();
        let status = objectArray.error == true ? 400 : 200;
        return res.status(status).json(objectArray.response);
    }

    public static getAllDatabase(): Promise<BasicAPIResponse> {
        return new Promise((resolve: any, reject: any) => {
            let query = "SELECT * FROM TASK";
            db.all(query, [], function (err: any, rows: Task[]) {
                let response;
                if (err) {
                    response = new BasicAPIResponse(err.stack, true);
                    reject(response);
                }
                response = new BasicAPIResponse(rows, false);
                resolve(response);
            });
        });
    }

    static async patch(req: Request, res: Response) {
        let taskToUpdate: Task = <Task> req.body;
        console.log(req.body);
        let response:any;
        if(taskToUpdate.hasOwnProperty('id') && 
          (taskToUpdate.hasOwnProperty('isDone') || taskToUpdate.hasOwnProperty('description') || taskToUpdate.hasOwnProperty('isUrgent')))
            response = await TaskController.updateDatabase(taskToUpdate);
        else
            response = new BasicAPIResponse("Dados insuficientes para alterar a tarefa!", true);
        
        let status = response.error == true ? 400 : 200;
        return res.status(status).json(response.response);
    }

    static async updateIsDone(req: Request, res: Response) {
        const id: number = parseInt(req.params.id);
        const isDone: boolean = req.params.bool == "true";

        let taskToUpdate: Task = <Task> {};
        taskToUpdate.id = id;
        taskToUpdate.isDone = isDone;
        console.log(`Update task ${id}, set isDone ${isDone}`);
        let response:any;
        if(taskToUpdate.hasOwnProperty('id') && taskToUpdate.hasOwnProperty('isDone'))
            response = await TaskController.updateDatabase(taskToUpdate);
        else
            response = new BasicAPIResponse("Dados insuficientes para alterar a tarefa!", true);
        
        let status = response.error == true ? 400 : 200;
        return res.status(status).json(response.response);
    }

    static async updateDatabase(objectToUpdate: Task): Promise<BasicAPIResponse> {
        return new Promise((resolve, reject) => {
            let query = `UPDATE TASK set `; 
            let params = [];
            if(objectToUpdate.hasOwnProperty('description')){
                query += "description = ?"
                params.push(objectToUpdate.description);
            }
            if(objectToUpdate.hasOwnProperty('isDone')){
                params.length != 0 ? query += ", isDone = ?" : query += "isDone = ?";
                params.push(objectToUpdate.isDone);
            }
            if(objectToUpdate.hasOwnProperty('isUrgent')){
                params.length != 0 ? query += ", isUrgent = ?" : query += "isUrgent = ?";
                params.push(objectToUpdate.isUrgent);
            }
            query += " where id=?";
            params.push(objectToUpdate.id);

            db.run(query, params, function (err: any) {
                let response: any;
                if (err) {
                    response = new BasicAPIResponse(err.stack, true);
                    reject(response);
                }
                response = new BasicAPIResponse("Alterado com sucesso!", false);
                resolve(response);
            });
        });
    }

    static async delete(req: Request, res: Response) {
        const id: number = parseInt(req.params.id);
        const response = await TaskController.deleteDatabase(id);
        return res.json(response);
    }

    static async deleteDatabase(id: number): Promise<Response> {
        return new Promise((resolve, reject) => {
            let query = `DELETE FROM TASK where id=?`;
            let params = [id];
            db.run(query, params, function (err: any) {
                let response: any;
                if (err) {
                    response = new BasicAPIResponse(err.stack, true);
                    reject(response);
                }
                response = new BasicAPIResponse("Deletado com sucesso!", false);
                resolve(response);
            });
        });
    }
}
