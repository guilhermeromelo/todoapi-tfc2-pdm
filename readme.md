Para rodar a API:<br>

1 - Tenha o Node.js instalado. (utilizei a versão v.14)<br>
2 - Abra o projeto e execute no nível do arquivo package.json o comando "npm install".<br>
3 - No mesmo terminal, execute o comando "npm start".<br>
4 - API rodando na porta 8100.

Autores: Guilherme Rodrigues de Melo e Maria Luísa de Carvalho Mardegan.